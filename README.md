# Jest Basics

This is a repo that takes you through some of the basic jest functions. Functions described in this are:

* Basic Expects
* Mock Functions
* Async Function Mocking
* Basic Component
* Component With State
