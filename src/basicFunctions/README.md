# Basic Functions

This folder contains tests for simple functions with basic functionality.

This form of testing is very common across many different JS testing libraries such as mocha and chai so this is useful for any library you decide to use for JS testing.