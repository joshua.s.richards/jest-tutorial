import { add, subtract, multiply, divide } from "./functions";

describe('functions', () => {
    describe('add', () => {
        it('adds 2 numbers', () => {
            expect(add(2, 3)).toEqual(5);
        });
    });

    describe('subtract', () => {
        it('subtracts the second parameter from the first', () => {
            expect(subtract(5, 2)).toEqual(3);
        });
    });

    describe('multiply', () => {
        it('multiplies 2 numbers', () => {
            expect(multiply(2, 3)).toEqual(6);
        });
    });

    describe('divide', () => {
        it('divides the first parameter by the second', () => {
            expect(divide(6, 3)).toEqual(2);
        });
        
        it('rounds the result to 2 decimal places', () => {
            expect(divide(1, 3)).toEqual(0.33);
        });
    });
});
