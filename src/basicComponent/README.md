# Basic Function

This shows a test for a static function. Since there is no functionality involved in this component, there is just a snapshot test.

This snapshot shows the html structure of the component in text form so you can see what your component adds to the DOM. This allows you to check that the component is rendering as you expect.