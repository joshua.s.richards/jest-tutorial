import React from "react";
import {shallow} from "enzyme";
import Logo from "./logo";

const getContainer = () => shallow(<Logo/>);

describe('Logo', () => {
    it('renders correctly', () => {
        expect(getContainer()).toMatchSnapshot();
    });
});