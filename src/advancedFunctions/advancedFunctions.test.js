import axios from "axios";
import * as functions from "../basicFunctions/functions";
import { getAreaOfCircle, getAxiosResult, getFetchResult } from "./advancedFunctions";

jest.mock('axios');

describe('advancedFunctions', () => {
    describe('getAreaOfCircle', () => {
        it('gets the area of a circle', () => {
            functions.multiply = jest.fn().mockReturnValue(9);
            expect(getAreaOfCircle(3)).toEqual(9*Math.PI);
            expect(functions.multiply).toHaveBeenCalledWith(3, 3);
        });
    });

    describe('getAxiosResult', () => {
        it('gets the axios result', async () => {
            axios.get.mockResolvedValue({data: 'axiosResult'});
            
            expect(await getAxiosResult()).toEqual('axiosResult');
        });
    });

    describe('getFetchResult', () => {
        it('gets the fetch result', async () => {
            window.fetch = jest.fn().mockResolvedValue({json: () => 'fetchResult'});

            expect(await getFetchResult()).toEqual('fetchResult');
        });
    });
});