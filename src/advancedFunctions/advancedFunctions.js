import axios from "axios";
import { multiply } from "../basicFunctions/functions";

export const getAreaOfCircle = radius => Math.PI*multiply(radius, radius);

export const getAxiosResult = async () => await axios.get('www.externalServer.com').then(response => response.data);

export const getFetchResult = async () => await fetch('www.externalServer.com').then(response => response.json());
