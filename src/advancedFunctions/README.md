# Advanced Functions

## Area Of A Circle
This test shows how to mock out a call to a function you have created in an external module.

It's not explicitly needed in this case as there are no side effects that could affect your test.

However this is good for an example.

## Getting XML Request
There are 2 popular ways to get requests from an external server, axios and fetch.

These are tests showing how to handle both of them since these will used by most people.

This test shows how to mock an imported module so that you can test the results of those calls.

This can then be used when testing other functions so you can test additional side effects.