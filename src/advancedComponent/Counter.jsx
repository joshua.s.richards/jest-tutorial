import React from "react";

const Counter = () => {
    const [counter, setCounter] = React.useState(0);

    React.useEffect(() => {
        setInterval(() => setCounter(counter + 1), 1000);
    });

    return (
        <div className="Counter">
            <div className="Counter__value">{counter}</div>
            <button onClick={() => setCounter(counter + 1)}/>
        </div>
    );
}

export default Counter;
