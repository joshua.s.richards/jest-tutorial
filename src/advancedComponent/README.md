# Testing Component With State

## Counter
The Counter component has a button that increases the count and the count also increases every second.

This shows how to test that the state updates both on button click and every second.

## Things to Remember
* Using enzyme for testing, you cannot check hook state directly so you need to look at side effects
* You need fake timers to make sure you are always aware of your state's value.