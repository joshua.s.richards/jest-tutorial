import React from "react";
import { shallow, mount } from "enzyme";
import { act } from "react-dom/test-utils";
import Counter from "./Counter";

const getShallowContainer = () => shallow(<Counter />);
const getMountedContainer = () => mount(<Counter />);

// To control our setInterval, we can use jest's fake timers.
jest.useFakeTimers();

describe('Counter', () => {
    it('renders correctly', () => {
        expect(getShallowContainer()).toMatchSnapshot();
    });

    it('updates the counter when the button is clicked', () => {
        const container = getShallowContainer();

        expect(container.find('.Counter__value').find('div').text()).toEqual('0');

        // We wrap everything that can cause state changes in act as this otherwise
        // this causes react errors
        act(() => {
            container.find('button').simulate('click');
        });

        expect(container.find('.Counter__value').find('div').text()).toEqual('1');
    });

    it('updates the counter every second', () => {
        // Enzyme currently cannot run useEffect in shallow components
        // therefore we need to use mount here
        const container = getMountedContainer();

        expect(container.find('.Counter__value').find('div').text()).toEqual('0');

        act(() => {
            jest.advanceTimersByTime(1000);
        });

        expect(container.find('.Counter__value').find('div').text()).toEqual('1');

        act(() => {
            jest.advanceTimersByTime(1000);
        });

        expect(container.find('.Counter__value').find('div').text()).toEqual('2');
    });
});
